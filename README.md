# Flectra Community / l10n-finland

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_fi_business_code_validate](l10n_fi_business_code_validate/) | 2.0.1.0.0| Adds business code (business id) validation for partners
[l10n_fi_banks](l10n_fi_banks/) | 2.0.1.0.0| Finnish banks and their addresses
[l10n_fi_payment_terms](l10n_fi_payment_terms/) | 2.0.1.0.0| Common Finnish invoice payment terms
[l10n_fi_edicode](l10n_fi_edicode/) | 2.0.1.0.0| Adds EDI code field and operators
[l10n_fi_business_code](l10n_fi_business_code/) | 2.0.1.0.0| Adds a business code (business id) for partners


